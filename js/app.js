const productsSelectElement = document.getElementById('productsSelect');
const bankBalanceElement = document.getElementById('bankBalance');
const getLoanBtnElement = document.getElementById('getLoanBtn');
const paidAmountElement = document.getElementById('paidAmount');
const bankBtnElement = document.getElementById('bankBtn');
const workBtnElement = document.getElementById("workBtn");
const repayLoanBtnElement = document.getElementById('repayLoanBtn');
const loanBalanceElement = document.getElementById('loanBalance');
const loanLabelElement = document.getElementById('loanLabel');
const payBalanceElement = document.getElementById("payBalance");
const featureElement = document.getElementById('feature');
const productTitleElement = document.getElementById('productTitle');
const productSpecElement = document.getElementById('productSpec');
const productPriceElement = document.getElementById('productPrice');
const productImageElement = document.getElementById('productImg');
const infoMessageElement = document.getElementById('infoMessage');
const infoHeaderElement = document.getElementById('infoHeader');

let currentBalance = 0.0;
let payBalance = 0.0;
let products = [];
let currentLoanBalance = 0.0;
const imageUrl = "https://noroff-komputer-store-api.herokuapp.com/";
let infoMessage = "";

repayLoanBtnElement.style.display = "none";
loanLabelElement.style.display = "none";
bankBalanceElement.innerText = currentBalance + " DKK";
loanBalanceElement.innerText = loanBalanceElement + " DKK";
payBalanceElement.innerText = payBalance + " DKK";

//pull data using fetch api
fetch('https://noroff-komputer-store-api.herokuapp.com/computers')
    .then(res => res.json())
    .then(data => products = data)
    .then(products => addProducts(products));

//Add products to select element
const addProducts = (products) => {
    productSpecElement.innerText = products[0].specs;
    productImageElement.src = imageUrl + products[0].image;
    productPriceElement.innerText = products[0].price + " DKK";
    productTitleElement.innerText = products[0].title;
    featureElement.innerText = products[0].description;
    products.forEach(product => {
        const productElement = document.createElement("option");
        productElement.value = product.id;
        productElement.appendChild(document.createTextNode(product.title));
        productsSelectElement.appendChild(productElement);
    });
}

//Display product feature in Features section.
const handleProductChange = e => {
    const selectedProduct = products[e.target.selectedIndex];
    featureElement.innerText = selectedProduct.description;
    productTitleElement.innerText = selectedProduct.title;
    productSpecElement.innerText = selectedProduct.specs;
    productPriceElement.innerText = selectedProduct.price + " DKK";;
    productImageElement.src = imageUrl + selectedProduct.image;
    var att = document.createAttribute("onerror");
    att.value = "this.src='../images/image-not-found.jpg'";
    productImageElement.setAttributeNode(att);
}
productsSelectElement.addEventListener("change", handleProductChange);

//show bank balance in Bank element

const handleGetLoan = e => {
    console.log(currentLoanBalance);
    if(currentLoanBalance <= 0 ){
        let userInput = window.prompt("Enter your desired loan amount below");
        if(userInput && !isNaN(userInput)){
            let desiredLoan = parseFloat(userInput);
            if( currentBalance !== 0 && desiredLoan !== 0 ){
                currentLoanBalance += desiredLoan;
                if(desiredLoan < (2 * currentBalance)){
                    currentBalance += desiredLoan;
                    updateCurrentBalance();
                    updateCurrentLoanBalance();
                    showHideLoan(currentLoanBalance);
                } else {
                    alert("You can not get a loan double than your current balance.");
                }
            } else {
                alert('You can not get a loan because your current balance is 0.');
            }       
        } else if (userInput === null){
            return;
        }
         else {
            alert('Please enter a valid amount.')
        }
    } else {
        alert("You have repay your current loan first. Your outstanding loan amount: "+ currentLoanBalance +".");
    }
}

//Handle work
const handleWork = e => {
    payBalance = payBalance + 100;
    //alert("Congratulation! you have earned 100 DKK. Your current balance: "+ payBalance + ".");
    updatePayBalance();
}

//Handle bank transfer
const handBankTransfer = e => {
    if(payBalance > 0 ){
        if(currentLoanBalance > 0){
            let repayLoanAount = payBalance * 10 / 100;
            payBalance -= repayLoanAount;
            //update current loan amount
            currentLoanBalance -= repayLoanAount;
            if(currentLoanBalance < 0 )
                currentLoanBalance = 0;
                showHideLoan(currentLoanBalance);
            updateCurrentLoanBalance();
        }
        currentBalance += payBalance;
        payBalance = 0;
        updatePayBalance();
        updateCurrentBalance();
    } else {
        alert("Bank transfer not possible! Your current pay balance is "+ payBalance +".")
    }
}

//Handle repay loan
const handleRepayLoan = e => {
    if(payBalance > 0) {
        //pay loan
        if( payBalance >= currentLoanBalance){
            const toPayAmount =  currentLoanBalance;
            console.log("to be paid "+ toPayAmount);
            payBalance -= toPayAmount;
            currentLoanBalance -= toPayAmount;
        } else {
            const toPayAmount = payBalance;
            currentLoanBalance -= toPayAmount;
            payBalance -= toPayAmount;
        }
        updatePayBalance();
        updateCurrentLoanBalance();
        showHideLoan(currentLoanBalance.toFixed(2));
      
    } else {
        alert('Reapaying loan is not possible. Your currnent pay balance is '+ payBalance + ".");
    }
}

//Handle buy now

const handleBuyNow = e => {
    let laptopPrice = parseFloat(productPriceElement.textContent);
    if( currentBalance < laptopPrice){
        infoMessageElement.innerText =  "You do not have enough money in the bank."
        infoHeaderElement.classList.replace('alert-success', 'alert-danger');
        $('#info-modal').modal('show');
    } else {
        currentBalance -= laptopPrice;
        updateCurrentBalance();
        infoMessageElement.innerText =  "Congratulations! your puchase was successful."
        infoHeaderElement.classList.replace('alert-danger', 'alert-success');
        $('#info-modal').modal('show');
    }
}

function updatePayBalance() {
    $('#payBalance').text(payBalance + " DKK");
}


function showLoanLabel() {
    loanLabelElement.style.display = "block";
}

function updateCurrentBalance() {
    $('#bankBalance').text(currentBalance + " DKK");
}

function updateCurrentLoanBalance() {
    $('#loanBalance').text(currentLoanBalance + " DKK");
}

function showHideLoan(res) {
    if(res > 0){
        loanLabelElement.style.display = "block";
        repayLoanBtnElement.style.display = "block";
        
    } else {
        loanLabelElement.style.display = "none";
        repayLoanBtnElement.style.display = "none";
        getLoanBtnElement.style.display = "none";
        getLoanBtnElement.style.display = "block";
    }
}

// function initializeStore() {
//     repayLoanBtnElement.style.display = "none";
//     loanLabelElement.style.display = "none";
//     bankBalanceElement.innerText = currentBalance + " DKK";
//     loanBalanceElement.innerText = loanBalanceElement + " DKK";
//     payBalanceElement.innerText = payBalance + " DKK";
// }

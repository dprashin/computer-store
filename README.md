# Computer Store Web Application
A dynamic webpage built using Javascript, HTML, CSS and Bootstrap. 

Features:

- A user is allowed to buy a laptop if they have enough Bank balance.
- A user can earn money by clicking Work button. Each time the button is clicked 100 DKK is deposited in the Pay account.
- A user can transfer money form Pay account to a personal account by clicking Bank button.
- A user can also apply for loan. However, a desired loan amount shouldn't be more than a double of current Bank balance.
- If a user has an outstanding loan:
    - A "Repay loan" button appears. This button can be used to repay loan if there is sufficient amount in the Pay balance.
    - For each bank tranfer made from the Pay account, 10% of the transfer amount is deducted to repay the loan.
- Laptops select box:
    - A number of products can be selected.
    - For each selection, corresponding laptop information like: features, image, sepcification and Price is displayed.
- Buying product
    - A user can purchase a product if they have a sufficient balance.

The webpage is deployed and hosted with Gitlab. It can accessed through: https://dprashin.gitlab.io/computer-store/.

## Getting Started 
- Clone to a local directory.
- Open folder/directory in Visual Studio Code.
- Run/Right clik -> Open with Live Server.


## Prerequisites

- VS Code or similar.
- Live Server Plugin installed on VS Code (optional).


